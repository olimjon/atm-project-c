﻿using ATM_11616969.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ATM_11616969
{
    public class BankCardController
    {
        // Loads list of cards, this will be used to load all bankcard objects into a list and return it
        public List<BankCard> LoadCards()
        {
            using (var db = new DatabaseContext())
            {
                return db.BankCard.Select(x => x).ToList();
            }
        }

        public BankCard LoadCard(long cardNumber) // Getting BankCard by cardnumber
        {
            using (var db = new DatabaseContext())
            {
                return db.BankCard.Where(x => x.bankCardNumber == cardNumber).Include(x => x.account).FirstOrDefault();
            }
        }
    }
}
