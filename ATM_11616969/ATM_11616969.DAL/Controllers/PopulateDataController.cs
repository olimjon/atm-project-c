﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_11616969.DAL
{
    public class PopulateDataBaseController
    {
        // This method is used to populate a database with a sample data
        public void Execute()
        {
            using (var db = new DatabaseContext())
            {
                if (db.User.Select(x => x).Count() == 0) // This checks if the database has been populated before
                {
                    string[] firstNames = { "Erlinda", "Latesha", "Wallace", "Albina", "Willian", "Elizebeth", "Hugo", "Bettyann", "Austin" };
                    string[] lastNames = { "Stanton", "Hampson", "Gillison", "Kromer", "Gerdes", "Balding", "Iorio", "Gandhi", "Armistead" };
                    DateTime[] dob = { new DateTime(1975, 09, 22), new DateTime(1976, 08, 30), new DateTime(1977, 01, 12), new DateTime(1977, 08, 05), new DateTime(1977, 10, 21), new DateTime(1977, 11, 25), new DateTime(1979, 09, 13), new DateTime(1979, 09, 14), new DateTime(1980, 05, 27) };
                    long[] bankCardNumbers = { 379612156078383, 377939527606929, 346283487276383, 348921515209474, 341916708072058, 379612156078311, 377939527606922, 346283487276333, 348921515209444 };
                    DateTime[] dateFrom = { new DateTime(2017, 05, 29), new DateTime(2018, 01, 01), new DateTime(2018, 02, 21), new DateTime(2018, 03, 08), new DateTime(2018, 03, 29), new DateTime(2018, 03, 30), new DateTime(2018, 04, 11), new DateTime(2018, 05, 16), new DateTime(2018, 09, 19) };
                    DateTime[] dateUntil = { dateFrom[0].AddYears(3), dateFrom[1].AddYears(3), dateFrom[2].AddYears(3), dateFrom[3].AddYears(3), dateFrom[4].AddYears(3), dateFrom[5].AddYears(3), dateFrom[6].AddYears(3), dateFrom[7].AddYears(3), dateFrom[8].AddYears(3) };
                    int[] ccv = { 111, 222, 333, 444, 555, 666, 777, 888, 999 };
                    int[] pin = { 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999 };
                    for (int i = 0; i < 9; i++)
                    {
                        Account account = new Account() // Creating account object and assigning values to its properties
                        {
                            credit = new Credit() { balance = 0 }, // Assiging properties in a referencing anonymous object class
                            savings = new Savings() { balance = 0 },
                            cheque = new Cheque() { balance = 0 }
                        };
                        db.User.Add(new User()
                        {
                            firstName = firstNames[i],
                            lastName = lastNames[i],
                            dob = dob[i],
                            account = account
                        });
                        db.SaveChanges();
                        db.BankCard.Add(new BankCard()
                        {
                            account = account,
                            bankCardNumber = bankCardNumbers[i],
                            dateFrom = dateFrom[i],
                            dateUntil = dateUntil[i],
                            ccv = ccv[i],
                            pin = pin[i]
                        });
                        db.SaveChanges();
                    }
                }
            }
        }
    }
}
