﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_11616969.DAL
{
    public class AccountController
    {
        // This method returns nothing, accepts integer amount
        public void Deposit(int amount)
        {
            // Creating a new instance of DatabaseContext() and assigning it to db variable
            using (var db = new DatabaseContext()) // using prevents the program from crashing, when a database error occurs.
            {
                var account = db.Account.FirstOrDefault(x => x.id == Session.id); // Getting an account data from the database by id
                switch (Session.accountType) // Getting current session, when a user clicks on savings or credit button, this will assign a status to the enum object accordingly
                {
                    case AccountType.Savings:
                        account.savings.balance += amount;
                        break;
                    case AccountType.Credit:
                        account.credit.balance += amount;
                        break;
                    default:
                        account.cheque.balance += amount;
                        break;
                }
                db.SaveChanges();
                db.Transaction.Add(new Transaction() { account = account, amount = amount });
                db.SaveChanges();
            }
        }

        // This method returns nothing, accepts integer amount
        public void Withdraw(int amount)
        {
            // Creating a new instance of DatabaseContext() and assigning it to db variable
            using (var db = new DatabaseContext()) // using prevents the program from crashing, when a database error occurs.
            {
                var account = db.Account.FirstOrDefault(x => x.id == Session.id); // Getting an account data from the database by id
                switch (Session.accountType)// Getting current session, when a user clicks on savings or credit button, this will assign a status to the enum object accordingly
                {
                    case AccountType.Savings:
                        account.savings.balance -= amount; // In case if the account type was selected as a savings, then from the current Account object the referencing savings object's balance will be deducted the value of method's parameter
                        break;
                    case AccountType.Credit:
                        account.credit.balance -= amount;
                        break;
                    default:
                        account.cheque.balance -= amount;
                        break;
                }
                db.SaveChanges(); // applying changes into the database
                db.Transaction.Add(new Transaction() { account = account, amount = amount }); // Adding the transaction into transaction table.
                db.SaveChanges(); // applying changes into the database
            }
        }

        // This method returns nothing, accepts decimal amount
        public void Transfer(decimal amount)
        {
            // Creating a new instance of DatabaseContext() and assigning it to db variable
            using (var db = new DatabaseContext()) // using prevents the program from crashing, when a database error occurs.
            {
                var account = db.Account.FirstOrDefault(x => x.id == Session.id);
                if (Session.transferFrom == AccountType.Savings && Session.transferTo == AccountType.Credit)
                {
                    account.savings.balance -= amount; // subtracting amount from the first selected account type
                    account.credit.balance += amount; // adding the same amount to the next selected account type
                }
                if (Session.transferFrom == AccountType.Credit && Session.transferTo == AccountType.Savings)
                {
                    account.credit.balance -= amount;
                    account.savings.balance += amount;
                }
                if (Session.transferFrom == AccountType.Savings && Session.transferTo == AccountType.Cheque)
                {
                    account.savings.balance -= amount;
                    account.cheque.balance += amount;
                }
                if (Session.transferFrom == AccountType.Cheque && Session.transferTo == AccountType.Savings)
                {
                    account.cheque.balance -= amount;
                    account.savings.balance += amount;
                }
                if (Session.transferFrom == AccountType.Credit && Session.transferTo == AccountType.Cheque)
                {
                    account.credit.balance -= amount;
                    account.cheque.balance += amount;
                }
                if (Session.transferFrom == AccountType.Cheque && Session.transferTo == AccountType.Credit)
                {
                    account.cheque.balance -= amount;
                    account.credit.balance += amount;
                }

                db.SaveChanges(); // applying changes into the database
                db.Transaction.Add(new Transaction() { account = account, amount = amount });
                db.SaveChanges(); // applying changes into the database
            }
        }
    }
}
