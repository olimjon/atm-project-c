﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ATM_11616969.DAL
{
    public class DatabaseContext : DbContext
    {
        // These properties will be used to talk to the database at the behind of the scenes
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Transaction> Transaction { get; set; }
        public virtual DbSet<BankCard> BankCard { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankCard>().HasIndex(x => x.bankCardNumber).IsUnique(); // Adding db constraint, to make bankcards unique
        }
    }
}
