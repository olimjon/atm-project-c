﻿// Helps to easily select account type
public enum AccountType
{
    Savings,
    Credit,
    Cheque
}