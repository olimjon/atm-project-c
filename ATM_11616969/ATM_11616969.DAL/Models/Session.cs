﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_11616969.DAL
{
    // This is an important class, which has static properties. These properties are alterable but constants, which can be shared by different classes
    public class Session
    {
        public static int id { get; set; }
        public static AccountType accountType { get; set; }
        public static AccountType transferFrom { get; set; }
        public static AccountType transferTo { get; set; }
    }
}
