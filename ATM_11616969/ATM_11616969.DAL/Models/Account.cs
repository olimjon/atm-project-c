﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_11616969.DAL
{
    public class Account
    {
        [Key] // This data annotation is required by Entity Framework to determine the primary key, when it will be mapping the models to entities
        public int id { get; set; }
        public Savings savings { get; set; } // References Savings class
        public Credit credit { get; set; } // References Savings class
        public Cheque cheque { get; set; } // References Savings class
    }
    // Please note, that an account can have 3 types of accounts and each of them has its own balance amount. Therefore, instead of using inheritance and
    // inheriting base class to other concrete classes, it is required to have them as a reference inside of the Account class
    public class Savings
    {
        public decimal balance { get; set; }
    }

    public class Credit
    {
        public decimal balance { get; set; }
    }

    public class Cheque
    {
        public decimal balance { get; set; }
    }
}
