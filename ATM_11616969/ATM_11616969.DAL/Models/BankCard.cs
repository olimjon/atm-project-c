﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_11616969.DAL
{
    public class BankCard
    {
        [Key] // This data annotation is required by Entity Framework to determine the primary key, when it will be mapping the models to entities
        public int id { get; set; }
        public long bankCardNumber { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateUntil { get; set; }
        public int ccv { get; set; }
        public int pin { get; set; }
        public virtual Account account { get; set; }
    }
}
