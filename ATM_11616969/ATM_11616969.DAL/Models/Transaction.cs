﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_11616969.DAL
{
    public class Transaction
    {// When there is only one integer property, Entity Framework will select it by default as a primary key
        public int id { get; set; }
        public Account account { get; set; } // References Account class
        public decimal amount { get; set; }
        public DateTime date { get { return DateTime.Now; } set { this.date = value; } }
    }
}
