﻿using ATM_11616969.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ATM_11616969
{
    #region Enumerations
    public enum Operation
    {
        Deposit,
        Withdraw,
        Transfer
    }
    public enum Visibility
    {
        Show,
        Hide
    }
    #endregion

    public partial class MainMenu : Form
    {
        #region Shared form Objects
        // Declaring objects which will be accessable within this class for other methods.
        BankCard card = new BankCard();
        AccountController account = new AccountController();
        List<Dictionary<Visibility, List<Control>>> deposit = new List<Dictionary<Visibility, List<Control>>>();
        List<Dictionary<Visibility, List<Control>>> withdraw = new List<Dictionary<Visibility, List<Control>>>();
        List<Dictionary<Visibility, List<Control>>> transfer = new List<Dictionary<Visibility, List<Control>>>();
        List<Dictionary<Visibility, List<Control>>> currentPath = new List<Dictionary<Visibility, List<Control>>>();
        int enteredCash = 0;
        Operation operation;
        
        int level = 0; 
        #endregion

        public MainMenu()
        {
            InitializeComponent();
            Init();
            PopulatePath();
        }

        #region Screen Level Control Builder

        #region Populating path, building navigation path
        // I have created a special List collection, where I included the path for each type of transaction method in dictionary collection. This will navigate use through certain steps
        // It can tell, which form Control object to hide, which on to show. The PopulatePathDictionary(left, right) - left side to show elements, right side to hide.
        public void PopulatePath()
        {
            deposit.Add(PopulatePathDictionary(ListOfControls(pMainMenu, pMessage, pNavigation, btnHelp), ListOfControls(btnBack)));
            deposit.Add(PopulatePathDictionary(ListOfControls(pNavigation, pMessage, pSelectAccount, btnBack), ListOfControls(btnHelp, btnConfirm)));
            deposit.Add(PopulatePathDictionary(ListOfControls(pPrint, pMessage, pNavigation, btnBack), ListOfControls(btnHelp, btnConfirm)));
            deposit.Add(PopulatePathDictionary(ListOfControls(pEnterCash, pMessage, pNavigation, btnConfirm), ListOfControls(btnBack, btnHelp)));
            deposit.Add(PopulatePathDictionary(ListOfControls(pMessage, pFinal)));

            withdraw.Add(PopulatePathDictionary(ListOfControls(pMainMenu, pMessage, pNavigation, btnHelp), ListOfControls(btnBack)));
            withdraw.Add(PopulatePathDictionary(ListOfControls(pEnterAmount, pMessage, pNavigation, btnConfirm), ListOfControls(btnHelp, btnBack)));
            withdraw.Add(PopulatePathDictionary(ListOfControls(pNavigation, pMessage, pSelectAccount, btnBack), ListOfControls(btnHelp, btnConfirm)));
            withdraw.Add(PopulatePathDictionary(ListOfControls(pPrint, pMessage, pNavigation, btnBack), ListOfControls(btnHelp, btnConfirm)));
            withdraw.Add(PopulatePathDictionary(ListOfControls(pMessage)));

            transfer.Add(PopulatePathDictionary(ListOfControls(pMainMenu, pMessage, pNavigation, btnHelp), ListOfControls(btnBack)));
            transfer.Add(PopulatePathDictionary(ListOfControls(pNavigation, pMessage, pSelectAccount, btnBack), ListOfControls(btnHelp)));
            transfer.Add(PopulatePathDictionary(ListOfControls(pNavigation, pMessage, pSelectAccount, btnBack), ListOfControls(btnHelp)));
            transfer.Add(PopulatePathDictionary(ListOfControls(pEnterAmount, pMessage, pNavigation, btnConfirm), ListOfControls(btnHelp, btnBack)));
            transfer.Add(PopulatePathDictionary(ListOfControls(pPrint, pMessage, pNavigation, btnBack), ListOfControls(btnHelp, btnConfirm)));
            transfer.Add(PopulatePathDictionary(ListOfControls(pMessage, pFinal)));

            currentPath.Add(PopulatePathDictionary(ListOfControls(pMainMenu, pMessage, pNavigation), ListOfControls(btnBack, pSelectCard)));
        }
        #endregion

        #region PopulatePathDictionary
        // This method populate a dictionary object and returns it. the dictionary contains 2 options: Visibility.show - visible items and visibility.hide - hiding objects.
        public Dictionary<Visibility, List<Control>> PopulatePathDictionary(List<Control> show, List<Control> hide = null)
        {
            Dictionary<Visibility, List<Control>> dic = new Dictionary<Visibility, List<Control>>();
            dic.Add(Visibility.Show, show);
            if(hide != null)
                dic.Add(Visibility.Hide, hide);
            return dic;
        }
        #endregion

        #region ListOfControls
        // Handful method which allows to add control objects as parameter, which in turn will return list of control objects
        public List<Control> ListOfControls(params Control[] objects)
        {
            List<Control> list = new List<Control>();
            for (int i = 0; i < objects.Length; i++)
            {
                list.Add(objects[i]);
            }
            return list;
        }
        #endregion

        #endregion

        #region Initialize Form Elements
        public void Init()
        {
            tbxMessage.Text = "Please Enter Card";
            foreach (Panel p in this.Controls)
            {
                if (p.Name != "pSelectCard" && p.Name != "pMessage")
                    p.Visible = false;
            }
            new PopulateDataBaseController().Execute();

            int i = 0;
            foreach (Control c in pSelectCard.Controls)
            {
                if (c is Button)
                    c.Text = new BankCardController().LoadCards().ElementAt(i).bankCardNumber.ToString();
                i++;
            }
        }
        #endregion

        #region Main method for controlling screen change
        public void ChangeScreen()
        {
            foreach (Panel p in this.Controls)
            {
                p.Visible = false;
            }
            List<Control> controlsToShow = new List<Control>();
            List<Control> controlsToHide = new List<Control>();
            currentPath.ElementAt(level).TryGetValue(Visibility.Show, out controlsToShow);
            currentPath.ElementAt(level).TryGetValue(Visibility.Hide, out controlsToHide);
            foreach (Control c in controlsToShow)
                c.Visible = true;
            if(controlsToHide != null)
                foreach (Control c in controlsToHide)
                    c.Visible = false;
        } 
        #endregion

        private void btnCard_Click(object sender, EventArgs e)
        {
            card = new BankCardController().LoadCard(long.Parse((sender as Button).Text)); // Getting bankcard number from this control element and loading account data using it, after assinging it to Bankcard an object
            Session.id = card.account.id;
            tbxMessage.Text = "Which transaction would you like to perform?";
            ChangeScreen();
        }

        private void btnCancelTransaction_Click(object sender, EventArgs e)
        {
            this.Dispose(); // releasing all resources used by the object
            this.Close(); // closing
        }

        private void btnDeposit_Click(object sender, EventArgs e)
        {
            operation = Operation.Deposit; // setting current shared enum object
            currentPath = deposit; // selecting the deposit dictionary list for the currentpath
            tbxMessage.Text = "You have selected Deposit \r\n Please select an account type.";
            level += 1;
            ChangeScreen();
        }

        private void btnWithdraw_Click(object sender, EventArgs e)
        {
            operation = Operation.Withdraw; // setting current shared enum object
            currentPath = withdraw;
            tbxMessage.Text = "You have selected Withdraw \r\n Please enter how much you wish to withdraw.";
            level += 1;
            ChangeScreen();
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            operation = Operation.Transfer; // setting current shared enum object
            currentPath = transfer;
            tbxMessage.Text = "You have selected Transfer \r\n Select account from.";
            level += 1;
            ChangeScreen();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            level -= 1;
            ChangeScreen();
        }
        // Depending on which operation was selected: deposit or withdraw or transfer a switch will tell the program which line of code to execute. the state is stored in operation enum object
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            switch (operation)
            {
                case Operation.Deposit:
                    tbxMessage.Text = "Your transaction complete, would you like to perform another transaction?";
                    account.Deposit(enteredCash);
                    break; // exits the switch
                case Operation.Withdraw:
                    tbxMessage.Text = "Please select account type";
                    break;
                default:
                    tbxMessage.Text = "Would you like to print a receipt?";
                    break;
            }
            level += 1; // this element is used to control the list index
            ChangeScreen(); // executing screen change method
        }

        private void btnSelectAccounts_Click(object sender, EventArgs e)
        {
            Session.accountType = (AccountType)Enum.Parse(typeof(AccountType), (sender as Button).Text);
            switch (operation)
            {
                case Operation.Deposit:
                    tbxMessage.Text = "Would you like to print the receipt?";
                    break;
                case Operation.Withdraw:
                    tbxMessage.Text = "Would you like to print the receipt?";
                    break;
                default:
                    if(level == 1)
                    {
                        Session.transferFrom = Session.accountType;
                        tbxMessage.Text = "Select account to.";
                    }
                    else
                    {
                        Session.transferTo = (AccountType)Enum.Parse(typeof(AccountType), (sender as Button).Text);
                        tbxMessage.Text = "Please enter how much you want to transfer";
                    }
                    break;
            }
            level += 1; // this element is used to control the list index
            ChangeScreen(); // executing screen change method
        }

        private void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            switch (operation)
            {
                case Operation.Deposit:
                    tbxMessage.Text = "Please enter Cash";
                    break;
                case Operation.Withdraw:
                    tbxMessage.Text = "Please take your cash. For other operation, you need to reinsert your card. To do so, please close and run the program again";
                    account.Withdraw((int)nudAmount.Value);
                    break;
                default:
                    tbxMessage.Text = "Your transaction complete, would you like to perform another transaction?";
                    account.Transfer((int)nudAmount.Value);
                    break;
            }
            
            level += 1; // this element is used to control the list index
            ChangeScreen(); // executing screen change method
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            enteredCash = 0;
            lblTotal.Text = "";
            level = 0; // this element is used to control the list index
            ChangeScreen(); // executing screen change method
        }
        // Methods below controls the shared integer object enterCash. On click they will be increased accordingly
        private void btn5_Click(object sender, EventArgs e)
        {
            enteredCash += 5;
            lblTotal.Text = "Total: " + enteredCash;
        }

        private void btn10_Click(object sender, EventArgs e)
        {
            enteredCash += 10;
            lblTotal.Text = "Total: " + enteredCash;
        }

        private void btn20_Click(object sender, EventArgs e)
        {
            enteredCash += 20;
            lblTotal.Text = "Total: " + enteredCash;
        }

        private void btn50_Click(object sender, EventArgs e)
        {
            enteredCash += 50;
            lblTotal.Text = "Total: " + enteredCash;
        }

        private void btn100_Click(object sender, EventArgs e)
        {
            enteredCash += 100;
            lblTotal.Text = "Total: " + enteredCash;
        }
    }
}
