﻿namespace ATM_11616969
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pSelectAccount = new System.Windows.Forms.Panel();
            this.btnSavings = new System.Windows.Forms.Button();
            this.btnCredit = new System.Windows.Forms.Button();
            this.btnCheque = new System.Windows.Forms.Button();
            this.pNavigation = new System.Windows.Forms.Panel();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnHelp = new System.Windows.Forms.Button();
            this.btnCancelTransaction = new System.Windows.Forms.Button();
            this.pMainMenu = new System.Windows.Forms.Panel();
            this.btnDeposit = new System.Windows.Forms.Button();
            this.btnCheckBalance = new System.Windows.Forms.Button();
            this.btnWithdraw = new System.Windows.Forms.Button();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.pPrint = new System.Windows.Forms.Panel();
            this.btnPrintReceipt = new System.Windows.Forms.Button();
            this.btnBalanceOnScreen = new System.Windows.Forms.Button();
            this.btnNoReceipt = new System.Windows.Forms.Button();
            this.pMessage = new System.Windows.Forms.Panel();
            this.tbxMessage = new System.Windows.Forms.TextBox();
            this.pEnterAmount = new System.Windows.Forms.Panel();
            this.nudAmount = new System.Windows.Forms.NumericUpDown();
            this.pFinal = new System.Windows.Forms.Panel();
            this.btnYes = new System.Windows.Forms.Button();
            this.btnNo = new System.Windows.Forms.Button();
            this.pSelectCard = new System.Windows.Forms.Panel();
            this.btnCard9 = new System.Windows.Forms.Button();
            this.btnCard8 = new System.Windows.Forms.Button();
            this.btnCard5 = new System.Windows.Forms.Button();
            this.btnCard7 = new System.Windows.Forms.Button();
            this.btnCard6 = new System.Windows.Forms.Button();
            this.btnCard4 = new System.Windows.Forms.Button();
            this.btnCard3 = new System.Windows.Forms.Button();
            this.btnCard2 = new System.Windows.Forms.Button();
            this.btnCard1 = new System.Windows.Forms.Button();
            this.pEnterCash = new System.Windows.Forms.Panel();
            this.btn20 = new System.Windows.Forms.Button();
            this.btn10 = new System.Windows.Forms.Button();
            this.btn100 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn50 = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.pSelectAccount.SuspendLayout();
            this.pNavigation.SuspendLayout();
            this.pMainMenu.SuspendLayout();
            this.pPrint.SuspendLayout();
            this.pMessage.SuspendLayout();
            this.pEnterAmount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAmount)).BeginInit();
            this.pFinal.SuspendLayout();
            this.pSelectCard.SuspendLayout();
            this.pEnterCash.SuspendLayout();
            this.SuspendLayout();
            // 
            // pSelectAccount
            // 
            this.pSelectAccount.Controls.Add(this.btnSavings);
            this.pSelectAccount.Controls.Add(this.btnCredit);
            this.pSelectAccount.Controls.Add(this.btnCheque);
            this.pSelectAccount.Location = new System.Drawing.Point(12, 200);
            this.pSelectAccount.Name = "pSelectAccount";
            this.pSelectAccount.Size = new System.Drawing.Size(552, 273);
            this.pSelectAccount.TabIndex = 0;
            // 
            // btnSavings
            // 
            this.btnSavings.Location = new System.Drawing.Point(199, 93);
            this.btnSavings.Name = "btnSavings";
            this.btnSavings.Size = new System.Drawing.Size(158, 54);
            this.btnSavings.TabIndex = 0;
            this.btnSavings.Text = "Savings";
            this.btnSavings.UseVisualStyleBackColor = true;
            this.btnSavings.Click += new System.EventHandler(this.btnSelectAccounts_Click);
            // 
            // btnCredit
            // 
            this.btnCredit.Location = new System.Drawing.Point(199, 153);
            this.btnCredit.Name = "btnCredit";
            this.btnCredit.Size = new System.Drawing.Size(158, 54);
            this.btnCredit.TabIndex = 0;
            this.btnCredit.Text = "Credit";
            this.btnCredit.UseVisualStyleBackColor = true;
            this.btnCredit.Click += new System.EventHandler(this.btnSelectAccounts_Click);
            // 
            // btnCheque
            // 
            this.btnCheque.Location = new System.Drawing.Point(199, 213);
            this.btnCheque.Name = "btnCheque";
            this.btnCheque.Size = new System.Drawing.Size(158, 54);
            this.btnCheque.TabIndex = 0;
            this.btnCheque.Text = "Cheque";
            this.btnCheque.UseVisualStyleBackColor = true;
            this.btnCheque.Click += new System.EventHandler(this.btnSelectAccounts_Click);
            // 
            // pNavigation
            // 
            this.pNavigation.Controls.Add(this.btnConfirm);
            this.pNavigation.Controls.Add(this.btnBack);
            this.pNavigation.Controls.Add(this.btnHelp);
            this.pNavigation.Controls.Add(this.btnCancelTransaction);
            this.pNavigation.Location = new System.Drawing.Point(12, 479);
            this.pNavigation.Name = "pNavigation";
            this.pNavigation.Size = new System.Drawing.Size(552, 60);
            this.pNavigation.TabIndex = 0;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(391, 3);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(158, 54);
            this.btnConfirm.TabIndex = 0;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(391, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 54);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(391, 3);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(158, 54);
            this.btnHelp.TabIndex = 0;
            this.btnHelp.Text = "Help";
            this.btnHelp.UseVisualStyleBackColor = true;
            // 
            // btnCancelTransaction
            // 
            this.btnCancelTransaction.Location = new System.Drawing.Point(3, 3);
            this.btnCancelTransaction.Name = "btnCancelTransaction";
            this.btnCancelTransaction.Size = new System.Drawing.Size(158, 54);
            this.btnCancelTransaction.TabIndex = 0;
            this.btnCancelTransaction.Text = "Cancel Transaction";
            this.btnCancelTransaction.UseVisualStyleBackColor = true;
            this.btnCancelTransaction.Click += new System.EventHandler(this.btnCancelTransaction_Click);
            // 
            // pMainMenu
            // 
            this.pMainMenu.Controls.Add(this.btnDeposit);
            this.pMainMenu.Controls.Add(this.btnCheckBalance);
            this.pMainMenu.Controls.Add(this.btnWithdraw);
            this.pMainMenu.Controls.Add(this.btnTransfer);
            this.pMainMenu.Location = new System.Drawing.Point(12, 200);
            this.pMainMenu.Name = "pMainMenu";
            this.pMainMenu.Size = new System.Drawing.Size(552, 273);
            this.pMainMenu.TabIndex = 0;
            // 
            // btnDeposit
            // 
            this.btnDeposit.Location = new System.Drawing.Point(3, 153);
            this.btnDeposit.Name = "btnDeposit";
            this.btnDeposit.Size = new System.Drawing.Size(158, 54);
            this.btnDeposit.TabIndex = 0;
            this.btnDeposit.Text = "Deposit";
            this.btnDeposit.UseVisualStyleBackColor = true;
            this.btnDeposit.Click += new System.EventHandler(this.btnDeposit_Click);
            // 
            // btnCheckBalance
            // 
            this.btnCheckBalance.Location = new System.Drawing.Point(3, 213);
            this.btnCheckBalance.Name = "btnCheckBalance";
            this.btnCheckBalance.Size = new System.Drawing.Size(158, 54);
            this.btnCheckBalance.TabIndex = 0;
            this.btnCheckBalance.Text = "Check Balance";
            this.btnCheckBalance.UseVisualStyleBackColor = true;
            // 
            // btnWithdraw
            // 
            this.btnWithdraw.Location = new System.Drawing.Point(391, 153);
            this.btnWithdraw.Name = "btnWithdraw";
            this.btnWithdraw.Size = new System.Drawing.Size(158, 54);
            this.btnWithdraw.TabIndex = 0;
            this.btnWithdraw.Text = "Withdraw";
            this.btnWithdraw.UseVisualStyleBackColor = true;
            this.btnWithdraw.Click += new System.EventHandler(this.btnWithdraw_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.Location = new System.Drawing.Point(391, 213);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(158, 54);
            this.btnTransfer.TabIndex = 0;
            this.btnTransfer.Text = "Transfer";
            this.btnTransfer.UseVisualStyleBackColor = true;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // pPrint
            // 
            this.pPrint.Controls.Add(this.btnPrintReceipt);
            this.pPrint.Controls.Add(this.btnBalanceOnScreen);
            this.pPrint.Controls.Add(this.btnNoReceipt);
            this.pPrint.Location = new System.Drawing.Point(12, 200);
            this.pPrint.Name = "pPrint";
            this.pPrint.Size = new System.Drawing.Size(552, 273);
            this.pPrint.TabIndex = 0;
            // 
            // btnPrintReceipt
            // 
            this.btnPrintReceipt.Location = new System.Drawing.Point(199, 93);
            this.btnPrintReceipt.Name = "btnPrintReceipt";
            this.btnPrintReceipt.Size = new System.Drawing.Size(158, 54);
            this.btnPrintReceipt.TabIndex = 0;
            this.btnPrintReceipt.Text = "Print receipt";
            this.btnPrintReceipt.UseVisualStyleBackColor = true;
            this.btnPrintReceipt.Click += new System.EventHandler(this.btnPrintReceipt_Click);
            // 
            // btnBalanceOnScreen
            // 
            this.btnBalanceOnScreen.Location = new System.Drawing.Point(199, 153);
            this.btnBalanceOnScreen.Name = "btnBalanceOnScreen";
            this.btnBalanceOnScreen.Size = new System.Drawing.Size(158, 54);
            this.btnBalanceOnScreen.TabIndex = 0;
            this.btnBalanceOnScreen.Text = "Balance on Screen";
            this.btnBalanceOnScreen.UseVisualStyleBackColor = true;
            this.btnBalanceOnScreen.Click += new System.EventHandler(this.btnPrintReceipt_Click);
            // 
            // btnNoReceipt
            // 
            this.btnNoReceipt.Location = new System.Drawing.Point(199, 213);
            this.btnNoReceipt.Name = "btnNoReceipt";
            this.btnNoReceipt.Size = new System.Drawing.Size(158, 54);
            this.btnNoReceipt.TabIndex = 0;
            this.btnNoReceipt.Text = "No receipt";
            this.btnNoReceipt.UseVisualStyleBackColor = true;
            this.btnNoReceipt.Click += new System.EventHandler(this.btnPrintReceipt_Click);
            // 
            // pMessage
            // 
            this.pMessage.Controls.Add(this.tbxMessage);
            this.pMessage.Location = new System.Drawing.Point(90, 15);
            this.pMessage.Name = "pMessage";
            this.pMessage.Size = new System.Drawing.Size(392, 182);
            this.pMessage.TabIndex = 0;
            // 
            // tbxMessage
            // 
            this.tbxMessage.BackColor = System.Drawing.SystemColors.Menu;
            this.tbxMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxMessage.Enabled = false;
            this.tbxMessage.Location = new System.Drawing.Point(3, 3);
            this.tbxMessage.Multiline = true;
            this.tbxMessage.Name = "tbxMessage";
            this.tbxMessage.ReadOnly = true;
            this.tbxMessage.Size = new System.Drawing.Size(386, 176);
            this.tbxMessage.TabIndex = 0;
            // 
            // pEnterAmount
            // 
            this.pEnterAmount.Controls.Add(this.nudAmount);
            this.pEnterAmount.Location = new System.Drawing.Point(12, 200);
            this.pEnterAmount.Name = "pEnterAmount";
            this.pEnterAmount.Size = new System.Drawing.Size(552, 273);
            this.pEnterAmount.TabIndex = 0;
            // 
            // nudAmount
            // 
            this.nudAmount.Location = new System.Drawing.Point(210, 21);
            this.nudAmount.Name = "nudAmount";
            this.nudAmount.Size = new System.Drawing.Size(120, 20);
            this.nudAmount.TabIndex = 0;
            // 
            // pFinal
            // 
            this.pFinal.Controls.Add(this.btnYes);
            this.pFinal.Controls.Add(this.btnNo);
            this.pFinal.Location = new System.Drawing.Point(12, 479);
            this.pFinal.Name = "pFinal";
            this.pFinal.Size = new System.Drawing.Size(552, 60);
            this.pFinal.TabIndex = 0;
            // 
            // btnYes
            // 
            this.btnYes.Location = new System.Drawing.Point(391, 3);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(158, 54);
            this.btnYes.TabIndex = 0;
            this.btnYes.Text = "Yes";
            this.btnYes.UseVisualStyleBackColor = true;
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // btnNo
            // 
            this.btnNo.Location = new System.Drawing.Point(3, 3);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(158, 54);
            this.btnNo.TabIndex = 0;
            this.btnNo.Text = "No";
            this.btnNo.UseVisualStyleBackColor = true;
            this.btnNo.Click += new System.EventHandler(this.btnCancelTransaction_Click);
            // 
            // pSelectCard
            // 
            this.pSelectCard.Controls.Add(this.btnCard9);
            this.pSelectCard.Controls.Add(this.btnCard8);
            this.pSelectCard.Controls.Add(this.btnCard5);
            this.pSelectCard.Controls.Add(this.btnCard7);
            this.pSelectCard.Controls.Add(this.btnCard6);
            this.pSelectCard.Controls.Add(this.btnCard4);
            this.pSelectCard.Controls.Add(this.btnCard3);
            this.pSelectCard.Controls.Add(this.btnCard2);
            this.pSelectCard.Controls.Add(this.btnCard1);
            this.pSelectCard.Location = new System.Drawing.Point(12, 200);
            this.pSelectCard.Name = "pSelectCard";
            this.pSelectCard.Size = new System.Drawing.Size(552, 339);
            this.pSelectCard.TabIndex = 0;
            // 
            // btnCard9
            // 
            this.btnCard9.Location = new System.Drawing.Point(346, 9);
            this.btnCard9.Name = "btnCard9";
            this.btnCard9.Size = new System.Drawing.Size(141, 60);
            this.btnCard9.TabIndex = 0;
            this.btnCard9.Text = "button1";
            this.btnCard9.UseVisualStyleBackColor = true;
            this.btnCard9.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard8
            // 
            this.btnCard8.Location = new System.Drawing.Point(199, 9);
            this.btnCard8.Name = "btnCard8";
            this.btnCard8.Size = new System.Drawing.Size(141, 60);
            this.btnCard8.TabIndex = 0;
            this.btnCard8.Text = "button1";
            this.btnCard8.UseVisualStyleBackColor = true;
            this.btnCard8.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard5
            // 
            this.btnCard5.Location = new System.Drawing.Point(199, 75);
            this.btnCard5.Name = "btnCard5";
            this.btnCard5.Size = new System.Drawing.Size(141, 60);
            this.btnCard5.TabIndex = 0;
            this.btnCard5.Text = "button1";
            this.btnCard5.UseVisualStyleBackColor = true;
            this.btnCard5.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard7
            // 
            this.btnCard7.Location = new System.Drawing.Point(52, 9);
            this.btnCard7.Name = "btnCard7";
            this.btnCard7.Size = new System.Drawing.Size(141, 60);
            this.btnCard7.TabIndex = 0;
            this.btnCard7.Text = "button1";
            this.btnCard7.UseVisualStyleBackColor = true;
            this.btnCard7.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard6
            // 
            this.btnCard6.Location = new System.Drawing.Point(346, 75);
            this.btnCard6.Name = "btnCard6";
            this.btnCard6.Size = new System.Drawing.Size(141, 60);
            this.btnCard6.TabIndex = 0;
            this.btnCard6.Text = "button1";
            this.btnCard6.UseVisualStyleBackColor = true;
            this.btnCard6.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard4
            // 
            this.btnCard4.Location = new System.Drawing.Point(52, 75);
            this.btnCard4.Name = "btnCard4";
            this.btnCard4.Size = new System.Drawing.Size(141, 60);
            this.btnCard4.TabIndex = 0;
            this.btnCard4.Text = "button1";
            this.btnCard4.UseVisualStyleBackColor = true;
            this.btnCard4.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard3
            // 
            this.btnCard3.Location = new System.Drawing.Point(346, 141);
            this.btnCard3.Name = "btnCard3";
            this.btnCard3.Size = new System.Drawing.Size(141, 60);
            this.btnCard3.TabIndex = 0;
            this.btnCard3.Text = "button1";
            this.btnCard3.UseVisualStyleBackColor = true;
            this.btnCard3.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard2
            // 
            this.btnCard2.Location = new System.Drawing.Point(199, 141);
            this.btnCard2.Name = "btnCard2";
            this.btnCard2.Size = new System.Drawing.Size(141, 60);
            this.btnCard2.TabIndex = 0;
            this.btnCard2.Text = "button1";
            this.btnCard2.UseVisualStyleBackColor = true;
            this.btnCard2.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCard1
            // 
            this.btnCard1.Location = new System.Drawing.Point(52, 141);
            this.btnCard1.Name = "btnCard1";
            this.btnCard1.Size = new System.Drawing.Size(141, 60);
            this.btnCard1.TabIndex = 0;
            this.btnCard1.Text = "button1";
            this.btnCard1.UseVisualStyleBackColor = true;
            this.btnCard1.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // pEnterCash
            // 
            this.pEnterCash.Controls.Add(this.lblTotal);
            this.pEnterCash.Controls.Add(this.btn20);
            this.pEnterCash.Controls.Add(this.btn10);
            this.pEnterCash.Controls.Add(this.btn100);
            this.pEnterCash.Controls.Add(this.btn5);
            this.pEnterCash.Controls.Add(this.btn50);
            this.pEnterCash.Location = new System.Drawing.Point(12, 200);
            this.pEnterCash.Name = "pEnterCash";
            this.pEnterCash.Size = new System.Drawing.Size(552, 270);
            this.pEnterCash.TabIndex = 0;
            // 
            // btn20
            // 
            this.btn20.Location = new System.Drawing.Point(352, 141);
            this.btn20.Name = "btn20";
            this.btn20.Size = new System.Drawing.Size(141, 60);
            this.btn20.TabIndex = 0;
            this.btn20.Text = "$20";
            this.btn20.UseVisualStyleBackColor = true;
            this.btn20.Click += new System.EventHandler(this.btn20_Click);
            // 
            // btn10
            // 
            this.btn10.Location = new System.Drawing.Point(205, 141);
            this.btn10.Name = "btn10";
            this.btn10.Size = new System.Drawing.Size(141, 60);
            this.btn10.TabIndex = 0;
            this.btn10.Text = "$10";
            this.btn10.UseVisualStyleBackColor = true;
            this.btn10.Click += new System.EventHandler(this.btn10_Click);
            // 
            // btn100
            // 
            this.btn100.Location = new System.Drawing.Point(280, 207);
            this.btn100.Name = "btn100";
            this.btn100.Size = new System.Drawing.Size(141, 60);
            this.btn100.TabIndex = 0;
            this.btn100.Text = "$100";
            this.btn100.UseVisualStyleBackColor = true;
            this.btn100.Click += new System.EventHandler(this.btn100_Click);
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(58, 141);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(141, 60);
            this.btn5.TabIndex = 0;
            this.btn5.Text = "$5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn50
            // 
            this.btn50.Location = new System.Drawing.Point(133, 207);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(141, 60);
            this.btn50.TabIndex = 0;
            this.btn50.Text = "$50";
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.Click += new System.EventHandler(this.btn50_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(254, 44);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(43, 13);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "Total: 0";
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 551);
            this.Controls.Add(this.pMessage);
            this.Controls.Add(this.pMainMenu);
            this.Controls.Add(this.pPrint);
            this.Controls.Add(this.pNavigation);
            this.Controls.Add(this.pSelectAccount);
            this.Controls.Add(this.pSelectCard);
            this.Controls.Add(this.pEnterAmount);
            this.Controls.Add(this.pFinal);
            this.Controls.Add(this.pEnterCash);
            this.Name = "MainMenu";
            this.Text = "ATM";
            this.pSelectAccount.ResumeLayout(false);
            this.pNavigation.ResumeLayout(false);
            this.pMainMenu.ResumeLayout(false);
            this.pPrint.ResumeLayout(false);
            this.pMessage.ResumeLayout(false);
            this.pMessage.PerformLayout();
            this.pEnterAmount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudAmount)).EndInit();
            this.pFinal.ResumeLayout(false);
            this.pSelectCard.ResumeLayout(false);
            this.pEnterCash.ResumeLayout(false);
            this.pEnterCash.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pSelectAccount;
        private System.Windows.Forms.Panel pNavigation;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Button btnCancelTransaction;
        private System.Windows.Forms.Panel pMainMenu;
        private System.Windows.Forms.Button btnDeposit;
        private System.Windows.Forms.Button btnCheckBalance;
        private System.Windows.Forms.Button btnWithdraw;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSavings;
        private System.Windows.Forms.Button btnCredit;
        private System.Windows.Forms.Button btnCheque;
        private System.Windows.Forms.Panel pPrint;
        private System.Windows.Forms.Button btnPrintReceipt;
        private System.Windows.Forms.Button btnBalanceOnScreen;
        private System.Windows.Forms.Button btnNoReceipt;
        private System.Windows.Forms.Panel pMessage;
        private System.Windows.Forms.Panel pEnterAmount;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.NumericUpDown nudAmount;
        private System.Windows.Forms.Panel pFinal;
        private System.Windows.Forms.Button btnYes;
        private System.Windows.Forms.Button btnNo;
        private System.Windows.Forms.Panel pSelectCard;
        private System.Windows.Forms.Button btnCard9;
        private System.Windows.Forms.Button btnCard8;
        private System.Windows.Forms.Button btnCard5;
        private System.Windows.Forms.Button btnCard7;
        private System.Windows.Forms.Button btnCard6;
        private System.Windows.Forms.Button btnCard4;
        private System.Windows.Forms.Button btnCard3;
        private System.Windows.Forms.Button btnCard2;
        private System.Windows.Forms.Button btnCard1;
        private System.Windows.Forms.Panel pEnterCash;
        private System.Windows.Forms.Button btn20;
        private System.Windows.Forms.Button btn10;
        private System.Windows.Forms.Button btn100;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn50;
        private System.Windows.Forms.TextBox tbxMessage;
        private System.Windows.Forms.Label lblTotal;
    }
}

